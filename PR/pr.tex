\documentclass[11pt,reqno]{amsart}
%\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
%\usepackage[latin1]{inputenc}
\usepackage{setspace}
\usepackage{textcomp}
\usepackage{path}
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{tikz}
%\usetikzlibrary{shapes,arrows}
\usetikzlibrary{shapes.geometric,arrows,positioning,fit,calc,graphs,trees}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=1cm, minimum height=1cm,text centered, draw=black, fill=gray!5, line width=2,text width=3cm,font=\footnotesize]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=1cm, minimum height=1cm, text centered, draw=black, fill=gray!20]
\tikzstyle{process} = [rectangle, minimum width=1cm, minimum height=1cm, text centered, draw=black, fill=white, text width=3cm,font=\scriptsize]
\tikzstyle{decision} = [diamond, minimum width=1cm, minimum height=1cm, text centered, draw=black, fill=gray!0,text width=3cm,font=\scriptsize]
\tikzstyle{arrow} = [thick,>=stealth',->]
\tikzstyle{circulo} = [circle,draw,font=\footnotesize,minimum width=1cm, text centered]
\tikzstyle{elipse} = [rectangle, rounded corners,draw,font=\footnotesize,minimum width=1cm, text centered]
%----------------------------

%\usepackage[colorlinks,backref]{hyperref}
\usepackage[backref]{hyperref}
%\usepackage{srcltx}
\usepackage{multicol}

\makeatletter
\renewcommand{\labelenumi}{\theenumi.}
\makeatother
\newcommand{\todo}[1]{}
%\newcommand{\todo}[1]{{\footnotesize \textbf{\textit{(TODO: #1)}}}} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\let\epsilon\varepsilon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newtheoremstyle{note}% name
  {4pt}%      Space above
  {4pt}%      Space below
  {\sl}%      Body font
  {}%         Indent amount (empty = no indent, \parindent = para indent)
  {\bfseries}% Thm head font
  {.}%        Punctuation after thm head
  {.5em}%     Space after thm head: " " = normal interword space;
                                %       \newline = linebreak
  {}%         Thm head spec (can be left empty, meaning `normal')

\newtheorem{theorem}             {Teorema}
\newtheorem{claim}[theorem] {Claim}
\newtheorem{lemma}      [theorem] {Lema}
\newtheorem{corollary} [theorem] {Corolário}
\newtheorem{fato}      [theorem] {Fato}
\newtheorem{conjectura}[theorem] {Conjectura}
\newtheorem{problem}  [theorem] {problem}

\theoremstyle{note}
\newtheorem{property}  [theorem] {Property}
\newtheorem{definicao}  [theorem] {Definition}
\newtheorem{exercise}  [theorem] {Exercise}

\makeatletter
\def\@setaddresses{\par
  \nobreak \begingroup
\footnotesize
  \def\author##1{\nobreak\addvspace\bigskipamount}%
  \def\\{\unskip, \ignorespaces}%
  \interlinepenalty\@M
  \def\address##1##2{\begingroup
    \par\addvspace\bigskipamount\indent
    \@ifnotempty{##1}{(\ignorespaces##1\unskip) }%
    {\scshape\ignorespaces##2}\par\endgroup}%
  \def\curraddr##1##2{\begingroup
    \@ifnotempty{##2}{\nobreak\indent{\itshape Current address}%
      \@ifnotempty{##1}{, \ignorespaces##1\unskip}\/:\space
      ##2\par}\endgroup}%
  \def\email##1##2{\begingroup
    \@ifnotempty{##2}{\nobreak\indent{\itshape Endereços Eletrônicos}%
      \@ifnotempty{##1}{, \ignorespaces##1\unskip}\/:\space
      \ttfamily##2\par}\endgroup}%
  \def\urladdr##1##2{\begingroup
    \@ifnotempty{##2}{\nobreak\indent{\itshape URL}%
      \@ifnotempty{##1}{, \ignorespaces##1\unskip}\/:\space
      \ttfamily##2\par}\endgroup}%
  \addresses
  \endgroup
}
\makeatother

\def\({\left(}
\def\){\right)}
\def\<{\langle}
\def\>{\rangle}
\let\:=\colon

\def\PP{{\mathbb P}}
\def\EE{{\mathbb E}}
\def\ZZ{{\mathbb Z}}
\def\NN{{\mathbb N}}
\def\QQ{{\mathbb Q}}

\def\cP{{\mathcal P}}
\def\cV{{\mathcal V}}
\def\cA{{\mathcal A}}
\def\cL{{\mathcal L}}


\doublespacing
\begin{document}
\title[Capture-aware algorithm for RFID systems]{%
{\sl Research Project}\\\medskip
A new anti-collision algorithm for passive RFID systems considering capture effect}

\author[Mota e Stagni]{%
   Rafael Perazzo Barbosa Mota,\\ Henrique Stagni}
%
\address{Instituto de Matem\'atica e Estat\'{\i}stica, Universidade de S\~ao
  Paulo, Rua do Mat\~ao 1010, 05508--090~S\~ao Paulo, SP}
%
\email{perazzo@ime.usp.br, stagni@ime.usp.br}


\begin{abstract}
Capture effect is a common phenomenon in passive radio frequency identification
systems. When multiple tags transmit their signals to a reader simultaneously,
one of the tags will be successfully identified due to capture effect. In this
project, we aim to develop an algorithm which can take advantage of a capture effect
environment to improve identification efficiency. We also require the algorithm to be easy to 
implement and computationally efficient. The starting point of the project is the 
IDFSA-IcolR algorithm, a Frame Slotted Algorithm which handle collisions as soon as they occur and 
which also uses a simpler approach for the number of tags estimation problem.
\end{abstract}

\maketitle
%\thispagestyle{empty}
\pagestyle{plain}
\footskip=20pt

\section{Introduction}
\label{sec:intro}

Radio Frequency Identification (RFID) is a generic term for technologies that
allows the identification of objects using radio waves. An unique number
identifying each object is stored in \emph{tag}. A tag is an electronic device
containing a microchip attached to an antenna. This device can receive a radio
signal from a \emph{reader} and then answer back with its identification number
and possibly other information. The reader is also an electronic device 
and it eventually sends the information provided by identified tags to some other 
device responsible to actually process this information.

The most used kind of tag is the lower-cost 
\emph{passive tag}, which does not have its own source of energy. Instead, 
energy is drawn from the electromagnetic created by the reader signal and 
this energy is used to power the microchip. This will be the only kind 
of tag considered in this work. 

The RFID technology provide many advantages over optical barcodes, such as not
requiring human intervention, being more robust (dirt, moisture, abrasion can
easily affect barcodes readability) and arguably more secure (since barcodes can
be easily counterfeited).  RFID has a broad range of applications, such as
supply chain management, toll systems, access control of people, electronic
passports and tickets, among many others (see \cite{SOMESURVEY} for a survey). 

One of the main current disadvantages in RFID systems is its low identification
efficiency, caused by \emph{tag collision}. Tag collision occurs when multiple 
tags answer simultaneously to the reader. Since they all share the same
communication channel, the radio signals containing each tag identification
number may interfere with each other, preventing proper identification. 

To deal with tag collision, the most used channel access method is Time 
Division Multiple Access (TDMA), mainly because tags do not have neither a 
radio transmitter in order to use Frequency Division Multiple Access (FDMA), nor 
enough computational power to perform Code Division Multiple Access (CDMA)
techniques. When using TDMA, communication is divided 
into different time \emph{slots}, so that tags transmitting on different 
time slots don't collide. 

Although there are many different TDMA based protocols, also called \emph{anticollision
algorithms}, few of them deal with a phenomenon called \emph{capture effect}.
Capture effect occurs when multiple tags transmit their signals on the same time
slot but, because of differences between each tag's signal strength, 
a single tag ``captures'' the slot, that is, can be
successfully identified by the reader. This effect can potentially increase the
performance of anticollision algorithms, since a tag can still be identified
even when other ones are sending their signals on the same slot.  However,
if a protocol is
unaware of capture-effect, it can fail to identify some tags. Moreover, many
anticollision algorithm must make an accurate estimation of the number of tags
in order to have a good performance, which implies that capture effect can
actually \emph{decrease} the throughput of unaware algorithms, since they will 
probably underestimate the number of tags.

In this project, we plan to investigate a capture-aware extension of  an
existing anticollision algorithm by Mota and Batista~\cite{mota:2014} called
Improved Dynamic Frame Slotted Aloha -- Instant Collision Resolution
(IDFSA-IcolR).

In the following section we clearly state the objective of this project. 
Section ~\ref{sec:background} contains a brief review of the research being done. 
It this section we also explain why an anticollision algorithm with the requirements 
we establish could be a valuable contribution to this area of research.

\section{Objective}
\label{sec:objective}
    Our objective is to propose a new RFID anticollision algorithm that
    \begin{enumerate}
        \item is \emph{capture-aware}, that is, one that does identify every tag even
        under capture-effect;
        \item takes advantage of capture effect to actually \emph{improve} identification
        efficiency;
        \item is easy to implement and does not require too much computational power.
    \end{enumerate}
    Moreover, we also plan to consider a more realistic model for the capture effect, instead 
    of assuming each collided slot is captured with a fixed probability. Although oblivious in the 
    literature, this later assumption is seen as an oversimplification of the actual phenomenon of
    capture effect.

    Despite the fact of not being capture-aware, IDFSA-IcolR anticollision algorithm has a 
    remarkable simple approach to estimate the total number of tags and thus it is the natural 
    starting point of our project.

\section{Background of the Proposed Research}
\label{sec:background}
    \subsection{Motivation}
    
    The Internet of Things (IoT) is a novel paradigm that is being developed in
    the modern scenario of wireless communications.  The basic idea of this
    concept is the pervasive presence around us of a variety of things or
    objects such as RFID tags, sensors, actuators,  mobile phones, and
    other technologies \cite{TODO1}.  This new communication paradigm is
    expected to make everyday life more sophisticated, more flexible, and
    easier to access any object across the world than with the current traditional
    Internet.  RFID systems allows to identify objects, record metadata,
    or control individual targets through radio waves. When RFID readers are
    connected to the Internet, they can identify, track and monitor the tagged
    objects.  When the tags reply simultaneously to the reader, collisions
    will happen because the reader identifies the tags through a shared
    wireless channel \cite{TODO11}.
    Therefore, RFID tag anticollision algorithms will play
    an important role in the IoT, since a large number of nodes
    is expected in this new paradigm.

    \subsection{Overview of anticollision algorithms}

    A widely used family of anticollision algorithms falls in the category of
    the so called Framed Slotted ALOHA (FSA) algorithms.  A FSA algorithm works
    by choosing the size $L$ of the \emph{frame}, and broadcasting a message so
    that each tag can assign itself to a uniformly random slot~$i$, $1\leq
    i\leq l$. Then, at each step $1\leq i\leq l$, the reader query every tag that has
    been assigned to $i$-th slot. If there is only one such tag, then it is
    successfully identified and we call such a slot an \emph{identification
    slot}. If multiple tags are assigned to the slot $i$, they collide and (in
    the absence of capture effect) none of them are identified and we call such
    a slot a \emph{collision slot}. Each FSA algorithm deal with collided tags
    in a different way. Some of them repeats this process until all tags are
    identified.  Others repeat the process with a \emph{different} frame size,
    according to the number of idle slots, collision slots and identification
    slots. Others, like IDFSA-IcolR, have a different approach: as soon as they
    detect a collision slot, a small frame is created to handle \emph{only}
    those tags that were assigned to it. Once they are all identified, the
    reader resumes the main identification procedure.

    Let's consider the efficiency of FSA algorithm. Suppose there are $n$ tags
    and that a reader has chosen a frame size of $L$. Then, a tag is assigned to
    a unique slot with probability
    $$\left(1-\frac1l\right)^{n-1},$$ which implies that the expected \emph{identification efficiency}, 
    i.e., the expected ratio between the number of identification slots and the
    total number of slots is given by
    $$\EE(eficiency) = \frac{n}l \left(1-\frac1l\right)^{n-1}.$$
    This efficiency is maximized when $l = n$, and its optimal value gets closer 
    to $e^{-1} \approx 36.79\%$ for large values of $n$.

    Hence, in order to reach good efficiency, FSA algorithms must first
    estimate the number $n$ of tags. There are multiple algorithm that can
    accurately estimate the number of tags, but many of them are
    computationally expensive.  For instance, both CMEBE\cite{TODO4} and
    Vogt\cite{TODO} algorithms estimate the number of tags by performing a
    number of brute force searches to maximize some measure of  the
    likelihood between the guessed and the observed distribution.  

    IDFSA-IcolR introduces a simpler approach to this problem, which demands
    less computational resources but still provides a reasonable identification
    efficiency.  This approach is described in Figure \ref{fig:estimation}.
    
            \begin{figure}[!htb]
            \centering
            \begin{tikzpicture}[->,>=stealth',auto,node distance=1cm,
              thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]

            \node[startstop](inicio){START \\Send $5$ Query(Q) commands};

            \node[decision,text width=1.5cm,below of=inicio,yshift=-1.5cm](cond1){$S_k>2$};
            \node[decision,text width=1.5cm,right of=cond1,xshift=2cm](cond2){$S_0>3$\\$S_k<2$};
            \node[process,left of=cond1,xshift=-2cm,text width=2cm](increment){$Q=Q+1$};
            \node[process,right of=inicio,xshift=2cm,text width=2cm](decrement){$Q=Q-1$};

            \node[process,below of=cond1,yshift=-1.5cm,text width=2cm](rebuttal){Rebuttal. Send 3 Query commands};
            \node[decision,text width=1.5cm,below of=rebuttal,yshift=-1.5cm](cond3){$S_k>1$};
            \node[process,left of=cond3,xshift=-2cm,text width=2cm](valorQ){$L=2^Q$};
            \node[process,right of=cond3,xshift=2cm,text width=2cm](valorMedio){$Q=int(\frac{2Q+1}{2})$\\$L=2^Q$};

            \node[startstop,below of=cond3,text width=1.5cm,yshift=-1.5cm](endestimation){END};

            %START DFSA

            \path[every node/.style={font=\sffamily\small}]
               (inicio) edge node [right] {} (cond1)
               (cond1) edge node[midway,above] {No} (cond2)
               (cond1) edge node[midway,above] {Yes} (increment)
               (cond2) edge node[midway,right] {No} (decrement)
               (decrement) edge node[midway,right] {} (inicio)
               (rebuttal) edge node[midway,right] {} (cond3)
               (cond3) edge node[midway,above] {No} (valorQ)
               (cond3) edge node[midway,above] {Yes} (valorMedio)
               ;

            \draw[arrow] (cond2.south) |-(rebuttal.east);
            \draw[arrow] (increment.north) |-(inicio.west);
            \draw[arrow] (valorQ.south) |-(endestimation.west);
            \draw[arrow] (valorMedio.south) |-(endestimation.east);
            %\draw[arrow] node[midway,above,yshift=-7.5cm,xshift=2cm]{Yes} (replies2.east) -|(framebig.south);
            %\draw[arrow] (success.west)-- ++(-5,0) |-(query.west);

            \end{tikzpicture}
            \caption{IDFSA-IcolR initial estimation flowchart.}
            \label{fig:estimation}
            \end{figure}
      
      
      At each
    step, the estimation procedure has a guess of~$L = 2^Q$ for the total
    number of tags.  Five \textsc{Query(Q)} commands are then broadcasted by
    the reader. When receiving each of these commands, a tag answers back to
    the reader with probability~$2^{-Q}$. If there were more than two
    \textsc{Query} commands such that multiple tags answered simultaneously ($S_k>2$), then
    the algorithm realizes it has underestimated its guess~$L$ for the number
    of tags. In that case this guess is increased and the procedure is repeated. In fact, if 
    the current guess~$L$ is such that~$n>2L$, then the probability of multiple tags answering 
    a single \textsc{Query} command is at least 
    $$1 - \left(1-\frac1L\right)^{2L} - 2\left(1-\frac1L\right)^{2L} \geq 1-\frac3{e^2} \approx 0.4.$$
    Therefore, in that case we would indeed expect multiple tags to answer back on at least three 
    out of five query commands. The reasoning is similar for the case in which the guess $L$ is 
    overestimated, since in that case we would expect answer from no tags for at least four out of 
    five \textsc{Query} commands ($S_0 > 3$).
    
    \subsection{Capture effect}  
        If capture effect occurs, some tags may not be identified at all when 
        using an algorithm that is not aware of the capture effect.
        For instance, it
        is easy to see that IDFSA-IcolR will never query any tag that has
        chosen a captured slot which means  all of those tags, except the one that actually 
        captured the slot, will never be identified.

        However, this is not the only challenge when considering capture effect. In fact, 
        most of the algorithms can be easily modified to deal with capture effect, since 
        it is assumed that reader can distinguish a captured slotted from a slot from a 
        \emph{proper} identification slot (we discuss this hypothesis on section \ref{sec:method}).
        What really matters when considering capture effect is how to take advantage of it in order 
        to increase identification efficiency. For instance, assume 
        every slot has a fixed probability $p$ of being captured by some tag assigned to it.
        In that case, the optimal frame size $l$ can be shown \cite{li_wang_iso180006c} to be 
        $l = p + (1-p)n$. Thus, a $FSA$ algorithm must either 
        assume $p$ to be known, or estimate it in order to select a good frame size. 
        
        Moreover, it may be an oversimplification to assume that every slot has a fixed probability 
        of being captured. We discuss another
        model for the capture effect that is arguably more realistic and, yet,
        potentially more suitable for IDFSA-IcolR-based algorithms
        \ref{sec:method}.

   \subsection{Gaps in existing Research}
        The phenomenon of capture effect on RFID systems has not being
        considered until recently.  Therefore most of the algorithms proposed
        in the literature do not account for capture effect and, thus, can have
        a reduced performance or, worse, fail to identify every tag. In that
        sense, further research is needed when considering  capture-aware
        anticollision algorithms.

        Besides that, most of the existing capture-aware solutions require too
        much computational power in order to estimate the number of tags (and
        other parameters related to capture effect). This may represent a
        problem in practice, since RFID readers are usually mobile devices
        themselves and hence should be assumed to have a somewhat limited
        computer power. 

        Finally, those few algorithm which do consider capture effect generally
        make use of an arguably unrealistic model of it, namely, to assume
        there is a fixed constant probability of a slot to be captured.

        Therefore, our work objective is aligned with the lack of easy to
        implement, computationally cheap and capture-aware RFID anticollision
        algorithms which can take advantage of a more realistic model of
        capture effect.

\section{Methodology}
\label{sec:method}
\subsection{Capture effect model}
We first plan to model the capture effect in the simplest possible way, namely,
to assume there is a fixed \emph{known} probability that some tag capture
the communication channel during a time slot.

On the other hand, we also plan to consider a more realistic model
of the capture effect, which takes the number of collided tags into account 
to determine the probability of one of them capturing the slot.  We briefly 
discuss this model in the remaining of this subsection.

It is somehow not intuitive that capture effect seems to happen more often when 
a slot has a small \emph{collision size}, i.e., if a small number of tags 
were assigned to it.  If we assume tags to be randomly placed inside a circular 
region on the plane, this phenomenon can actually be analytically verified when 
using two different stochastic models for radio propagation \cite{BLA,BLAA}.
More precisely, according to these aforementioned works, 
the capture effect probability decreases \emph{exponentially} with the 
collision size. In other words, we can assume there is a positive constants 
$\gamma$ such that the capture probability $C(s)$ of a slot with collision 
size $s$ is given by 
    $$C(s) = \frac{e^{-\gamma s}}{e^{-\gamma}}.$$

Not only is this a more accurate model of the capture effect, it can also 
improve the efficiency of our solution. In fact, provided we made a good 
estimation of the number of tags, it can be shown that with high probability 
the collision size of a slot is small 
(either $2$ or $3$). Not only that, but this model can 
potentially allow us to make a better estimation of the number of tags, 
since it provides additional ``pieces of information'' about a slot collision size.

\subsection{Implementation details and practical considerations}
In the absence of a widely used environment for RFID simulation, 
we plan to implement and simulate our extensions of the  IDFSA-IcolR algorithm 
from scratch, using \emph{GNU R} --- a programming language and software 
environment for statistical computing. Although our project does not require
advanced statistical techniques --- we are basically dealing with very
basic discrete distributions ---, R's extremely high standard of quality 
allows us not to worry about technical details (such as numerical accuracy) when 
performing statistical computing.

Most of the anticollision protocols make a number of assumptions about 
the environment. For instance, it is generally assumed that the reader 
can distinguish a slot in which capture effect occurs from
the case in which it was indeed assigned to a single tag.
 In practice, this process involves extra 
communication messages which are generally not accounted for when 
evaluating those algorithm performance.  

In order to allow a fair comparison between our protocol and the existing
solutions, we also make such assumptions. We plan, however, to keep track
of all the communication messages being done in the whole process. Firstly,
because it is obviously important not to loose track of all the 
inherent communication overhead when designing a new protocol.
Secondly, because by doing so
we can later
speculate about the performance of our solution when considering 
a more realistic measure of RFID system performance in which this 
overhead would be taken into consideration.

\section{Performance measure}
The usual measure for evaluating a RFID system performance is the 
\emph{slot efficiency}, defined as the ratio between the number of 
identified tags and the number of time slots spent on their identification.

We also plan to account for the actual time spend during the whole process.
This is not a redundant measure since in practice the time slots do not have
the same duration. For instance, RFID readers are able to detect a collision
before reading the whole signal which means the expected time spent on a slot
with collision is less than the time spent on a successful slot. A similar
phenomenon happens with slots in which no tags were assigned to. Although those
time values differs according to the hardware being used, it can be interesting
to evaluate the performance of our solution using the metric that would
probably be used if we were in a real, non simulated, environment. 


\section{Work plan}

Consider the following tasks needed to successfully complete this research project.
\begin{description}
    \item[A] Modify IDFSA-IcolR algorithm, so that it identifies every tag even 
    when capture effect occurs.
    \item[B] Research more realistic models of capture effect.
    \item[C] Calculate the optimal frame size of the selected capture effect model and 
    modify the algorithm to select this optimal size instead of $n$.
    \item[D] Modify the estimation of total number of tags, so that it can take advantage of 
    the capture effect chosen model.
    \item[E] Evaluate results and write article.
\end{description}

We plan to work on each of those tasks according to the following schedule.

\begin{tabular}{|l | c | c | c | c | c |}
\hline
   \emph{Schedule}& \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} \\
\hline   \textbf{September} & $\times$&$\times$ & & & \\
\hline   \textbf{October} & & &$\times$ &$\times$ & $\times$  \\
\hline   \textbf{November} & & & &$\times$ &$\times$ \\
          \hline  
          \end{tabular}
\label{crono}

%\bibliographystyle{plain}
%\bibliography{bibliografia,refs}

\end{document}
