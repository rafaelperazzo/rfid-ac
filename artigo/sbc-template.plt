set encoding utf8
unset label
set key left top
set xlabel "Quantidade de etiquetas"
set ylabel "Percentual de perdas"
set autoscale
set mxtics
set mytics
set format y "%g %%"
set grid xtics ytics lt 2 lw 0.5 lc rgb "#BEBEBE"
largura_linha=3
largura_ic=2
#Tabela de cores: http://gucky.uni-muenster.de/cgi-bin/rgbtab-en
#Linhas e pontos: http://sparky.rice.edu/gnuplot.html
#Linha normal
set style line 1 lc rgb 'black' lt -1 lw 1  pt 7 ps 1
#Linha com mecanismo
set style line 2 lc rgb '#292929' lt 5 lw 3 pt 7 ps 0.8
#Linha dos ICs
set style line 3 lc rgb 'black' lt 5 lw 3 pt 1 ps 0.5
set terminal postscript eps
#######################################################
set output "figuras/cenario1.eps"
set border linewidth 3
set label "IC 95%" at 30,40
set xrange [0:460]
set xtics 30,50,430
set ytics 3
plot 'dat/plot_1.0.perdas.dat' using 1:($2*100):($3*100):($4*100) with linespoint ls 1 title "Cenário 1 sem mecanismo", 'dat/plot_1.0.perdas.dat' using 1:($2*100):($3*100):($4*100) with errorbars ls 3 notitle, 'dat/plot_1.1.perdas.dat' using 1:($2*100):($3*100):($4*100) with linespoint title "Cenário 1 com mecanismo"  ls 2, 'dat/plot_1.1.perdas.dat' using 1:($2*100):($3*100):($4*100) with errorbars ls 3 notitle
#replot
#######################################################
set output "figuras/cenario2.eps"
set xtics 50,100,1050
set ytics 2
unset label
set label "IC 95%" at 60,16 
set xrange [0:1100]
plot 'dat/plot_2.0.perdas.dat' using 1:($2*100):($3*100):($4*100) with linespoint title "Cenário 2 sem mecanismo" ls 1, 'dat/plot_2.0.perdas.dat' using 1:($2*100):($3*100):($4*100) with errorbars ls 3 notitle, 'dat/plot_2.1.perdas.dat' using 1:($2*100):($3*100):($4*100) with linespoint title "Cenário 2 com mecanismo" ls 2, 'dat/plot_2.1.perdas.dat' using 1:($2*100):($3*100):($4*100) with errorbars ls 3 notitle
#replot
#######################################################
unset format
set output "figuras/trafego1.eps"
set ylabel "Kbytes"
set border linewidth 4
unset label
set label "IC 95%" at 30,9000
set xrange [0:460]
set xtics 30,50,460
set ytics 500
plot 'dat/plot_1.0.trafego.dat' using 1:2:3:4 with linespoint title "Cenário 1 sem mecanismo"  ls 1, 'dat/plot_1.0.trafego.dat' using 1:2:3:4 with errorbars ls 1 notitle, 'dat/plot_1.1.trafego.dat' using 1:2:3:4 with linespoint title "Cenário 1 com mecanismo" ls 2, 'dat/plot_1.1.trafego.dat' using 1:2:3:4 with errorbars ls 3 notitle
#replot
unset label
#######################################################
set output "figuras/trafego2.eps"
set xtics 50,100,1100
unset label
set label "IC 95%" at 60,3170
set xrange [0:1100]
plot 'dat/plot_2.0.trafego.dat' using 1:2:3:4 with linespoint title "Cenário 2 sem mecanismo" ls 1, 'dat/plot_2.0.trafego.dat' using 1:2:3:4 with errorbars ls 3 notitle, 'dat/plot_2.1.trafego.dat' using 1:2:3:4 with linespoint title "Cenário 2 com mecanismo" ls 2, 'dat/plot_2.1.trafego.dat' using 1:2:3:4 with errorbars ls 3 notitle
#replot
#######################################################
#reset
#set output "figuras/barras.eps"
#set xtics 20
#set style line 1 lc rgb "red"
#set boxwidth 0.7 relative
#set style fill solid 1.0
#set nokey
#set border linewidth 4
#plot 'dados.dat' using 1:3:xtic(2) with boxes
#replot
