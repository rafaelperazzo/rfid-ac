# LaTeX2HTML 2008 (1.71)
# Associate images original text with physical files.


$key = q/includegraphics[scale=0.3]{figurasslashfig_diagrama.eps};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="310" HEIGHT="213" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="\includegraphics[scale=0.3]{figuras/fig_diagrama.eps}">|; 

$key = q/r_k;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$ r_k$">|; 

$key = q/displaystyleTp=dfrac{sumlimits_{i=1}^{20}(dfrac{sumlimits_{j=1}^{ql}(dfrac{sumli{20}%Qt=dfrac{sumlimits_{i=1}^{20}(dfrac{(d+r)*8}{1000})}{20};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="239" HEIGHT="221" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$\displaystyle Tp=\dfrac{\sum\limits_{i=1}^{20}(\dfrac{\sum\limits_{j=1}^{ql}(\d...
...{s})}{ql})}{20} %Qt=\dfrac{\sum\limits_{i=1}^{20}(\dfrac{(d+r)*8}{1000})}{20}
$">|; 

$key = q/includegraphics[scale=0.4]{figurasslashclasses.eps};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="664" HEIGHT="233" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="\includegraphics[scale=0.4]{figuras/classes.eps}">|; 

$key = q/d_k;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$ d_k$">|; 

$key = q/includegraphics[scale=1.05]{figurasslashcenario1.eps};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="580" HEIGHT="412" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="\includegraphics[scale=1.05]{figuras/cenario1.eps}">|; 

$key = q/{resumo}ApesardosavançosnodesenvolvimentodenovosmecanismosparaaInternetdasCoisasdeperdadepacotesqueerade10%a42%foireduzidaparamenosde3%.{par{{resumo};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="485" HEIGHT="292" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="\begin{resumo}
Apesar dos avanços no desenvolvimento de novos mecanismos para a ...
...pacotes que era de 10\% a 42\% foi reduzida para menos de 3\%.
\par
\end{resumo}">|; 

$key = q/d_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ d_i$">|; 

$key = q/includegraphics[scale=1.05]{figurasslashtrafego2.eps};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="580" HEIGHT="412" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="\includegraphics[scale=1.05]{figuras/trafego2.eps}">|; 

$key = q/displaystyleQt=dfrac{mathlargersumlimits_{i=1}^{20}(dfrac{(d_i+r_i)*8}{1000})}{20};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="195" HEIGHT="141" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$\displaystyle Qt=\dfrac{\mathlarger\sum\limits_{i=1}^{20}(\dfrac{(d_i+r_i)*8}{1000})}{20}$">|; 

$key = q/r_i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$ r_i$">|; 

$key = q/{algorithm}%latex2htmlidmarker168[!htb]Entrada{Pacotedesolicitaçãodoleitor}Saidaoritmoanti-colisãoprobabilísticoimplementadonasetiquetas}{{algorithm};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="214" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="\begin{algorithm}
% latex2html id marker 168
[!htb]
\Entrada{Pacote de solicitaç...
...lgoritmo anti-colisão probabilístico implementado nas etiquetas}
\end{algorithm}">|; 

$key = q/includegraphics[scale=1.05]{figurasslashcenario2.eps};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="580" HEIGHT="412" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="\includegraphics[scale=1.05]{figuras/cenario2.eps}">|; 

$key = q/includegraphics[scale=1.05]{figurasslashtrafego1.eps};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="580" HEIGHT="412" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="\includegraphics[scale=1.05]{figuras/trafego1.eps}">|; 

$key = q/{algorithm}%latex2htmlidmarker84[!htb]Entrada{Pacotedesolicitaçãodoleitor}Saida{QoSemaplicaçõesdaIoTpararastreamentoelocalização}{{algorithm};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="555" HEIGHT="629" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="\begin{algorithm}
% latex2html id marker 84
[!htb]
\Entrada{Pacote de solicitaçã...
...tia de QoS em aplicações da IoT para rastreamento e localização}
\end{algorithm}">|; 

$key = q/includegraphics[scale=0.25]{figurasslashfig8.eps};LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="627" HEIGHT="292" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="\includegraphics[scale=0.25]{figuras/fig8.eps}">|; 

1;

