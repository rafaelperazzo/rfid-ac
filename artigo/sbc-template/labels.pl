# LaTeX2HTML 2008 (1.71)
# Associate labels original text with physical files.


$key = q/cite_duan:2011/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_atzori:2010/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_liu:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_miorandi:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:conclusoes/;
$external_labels{$key} = "$URL/" . q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:mecanismo/;
$external_labels{$key} = "$URL/" . q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_han:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_evdokimov:2011/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:protocolo/;
$external_labels{$key} = "$URL/" . q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_leonardo:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:8/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:trafego1/;
$external_labels{$key} = "$URL/" . q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:resultados/;
$external_labels{$key} = "$URL/" . q|node6.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:classes/;
$external_labels{$key} = "$URL/" . q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/alg:col/;
$external_labels{$key} = "$URL/" . q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:cenario1/;
$external_labels{$key} = "$URL/" . q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/alg:sing/;
$external_labels{$key} = "$URL/" . q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:cenario2/;
$external_labels{$key} = "$URL/" . q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_finkenzeller:2010/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_pal:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:trafego2/;
$external_labels{$key} = "$URL/" . q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_welbourne:2009/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:introducao/;
$external_labels{$key} = "$URL/" . q|node1.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_nef:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_chen:2007/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:cenarios/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_weifeng:2012/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_haifeng:2013/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/ns2/;
$external_labels{$key} = "$URL/" . q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:1/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:motivacoes/;
$external_labels{$key} = "$URL/" . q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:trabalhos/;
$external_labels{$key} = "$URL/" . q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:2/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_epc:2008/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2008 (1.71)
# labels from external_latex_labels array.


$key = q/fig:cenario2/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/fig:trafego2/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/sec:conclusoes/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/sec:introducao/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/fig:mecanismo/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/sec:protocolo/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/sec:cenarios/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/fig:8/;
$external_latex_labels{$key} = q|2|; 
$noresave{$key} = "$nosave";

$key = q/ns2/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/fig:trafego1/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/eq:1/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/sec:resultados/;
$external_latex_labels{$key} = q|6|; 
$noresave{$key} = "$nosave";

$key = q/fig:classes/;
$external_latex_labels{$key} = q|3|; 
$noresave{$key} = "$nosave";

$key = q/sec:motivacoes/;
$external_latex_labels{$key} = q|2|; 
$noresave{$key} = "$nosave";

$key = q/alg:col/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/sec:trabalhos/;
$external_latex_labels{$key} = q|3|; 
$noresave{$key} = "$nosave";

$key = q/eq:2/;
$external_latex_labels{$key} = q|2|; 
$noresave{$key} = "$nosave";

$key = q/fig:cenario1/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/alg:sing/;
$external_latex_labels{$key} = q|17|; 
$noresave{$key} = "$nosave";

1;

